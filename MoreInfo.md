## Introduction

This document contains information on how to properly install
all the necessary components for the Lane Stadium Sensor Node. 
A RaspberryPi will be required as well as a DHT-22 temperature/
humidity sensor, a Sparkfun sound sensor, and an Arduino Uno.

## Set-up

In the Setup.md file, there is a list of commands that need
to be executed on the RaspberryPi terminal. Once all of the
commands have been executed, the RaspberryPi should be ready
for code. The main program is a python file called, Sensor.py
and should be put in a folder in the within the /home/pi 
directory.

On line 23 of the Sensor.py program, there is a url for the
Rest API with a missing key. In order to obtain the key, you
will need to contact either Thomas Weeks (Tweeks) or Tommy
Regan. The contact information for both of them will be
listed at the bottom of this file. Once you have access to 
the key follow the directions in the Key.md file to insert
it into the Sensor.py code (located in Key.zip).

## Start-up Script

If you would like the code to run on start-up, go to the
file named startup.sh. The first two lines of startup.sh let
the pi know what type of file this is and how it should be read.
In this file you will notice that there is reference to a file
called uploadDHT22Data.py. This is the exact same thing as 
Sensor.py, but it has a different name. The way this file works,
is it changes from the home directory to whichever directory
your Sensor.py file is in. Then it calls your Sensor.py file and
starts running it. On the line that starts "cd /home/pi/" finish
the line after the forward slash with the directory that contains
your Sensor.py file.

Once this is is finished, save it in your /home/pi/ directory.
Now go to the /etc/rc.local file on the RaspberryPi. Add the
directory of your startup.sh file (your startup script) to the
file. DO NOT change anything else. Anything that is commented our
should be left commented out. When you insert your directory to
your startup.sh file, you should put it after the

```
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
```

portion of the file and before the "exit 0" portion of the file.
You should not comment out the "exit 0". Right after your absolute
directory path to your startup.sh file, you should add an ampersand.
An example is shown below, and for further reference look at the 
sample file named rc_local.sh. The purpose of the ampersand (&) is
to ensure that your startup.sh script is running in the background,
which also means that your Sensor.py file is also running in the
background, since it's on an infinite loop. This will prevent the 
pi from crashing and ensuring that your program works properly.

```
# By default this script does nothing.

/home/pi/startup.sh &

exit 0
```

## Troubleshooting

If you forget to include the ampersand at the end of your directory,
you will have to power down the pi and remove the SD card. Once you
have removed the SD card, put it into your computer and go to the
file called /boot/cmdline.txt. At the very end of that file add the
line "init=/bin/bash" without the quotation marks. This should allow
you to edit files on your RaspberryPi, but it will not give it full
functionality. Next, remove the SD card from your computer and plug
it back into your RaspberryPi. Edit whichever file is causing an
issue (most likely rc.local) and save your changes.

Once you think you have fixed the problem, power-down the pi and remove
the SD card. Plug the SD card back into your computer, go back to the
/boot/cmdline.txt file and remove the part you added (init=/bin/bash).
Now take the SD card and put it back into the pi and everything should
be working properly.

If you have any questions, comments, or concerns, you may contact
Thomas Weeks (Tweeks) or Google any issues you may have.

## Points of Contact

* Thomas “Tweeks” Weeks  
Director, Technology Futures and Community Advocacy  
Division of Information Technology, Virginia Tech  
(w) 540-231-7893 (m) 210-264-1185  
t.weeks@vt.edu  

* Tommy Regan  
Senior Director, Information Systems   
(w) 540-231-7539  
tregan@vt.edu  