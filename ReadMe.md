![Virginia Tech Information Technology Department Logo](https://image.ibb.co/cNyzTc/VTIT_Logo2.jpg)

# About this Repo

ECE 4805/4806 Lane Stadium Sensor Group

This project is part of the ECE 4805/4806 senior design experience course taught
by Kenneth Schulz. This group's project was to create a real time sensor node
capable of reporting data to a cloud, in this case Microsoft Power BI. For this,
the team used an Arduino Uno to read analog data from a Sparkfun Sound sensor and
then used a serial connection to report this data to a Raspberry Pi 3. The Raspberry 
Pi took this data, along with data from a DHT-22 sensor connected to the Pi, and
sent them to a rest API accessible by Microsoft Power BI. This software then allows 
users access to a multi-device friendly dashboard from which both a live steam 
and history of data are available. 

## Contents
- Code to program Sensors
- A list of commands to install add-ons on the Raspberry Pi
- Code for a start-up script
- A key to access the Rest API
- Extra information and instructions

## Usage

This should be used to observe temperature, humidity, and noise data in Lane Stadium.

## Logging into the Pi

If you want to login in to the Raspberry Pi sensor node, you will have to contact either
Tweeks or Tommy Regan in order to get the username and password. You will also need the
IP address and MAC address to be able to SSH into the pi. Both are listed below.

* IP Address: 128.173.122.131  
* MAC Address: b827.eb11.c6bc  

## Block Diagram

![Sensor Node Block Diagram](https://image.ibb.co/i17zBx/Block_Diagram.png)

## Points of Contact

* Thomas “Tweeks” Weeks  
Director, Technology Futures and Community Advocacy  
Division of Information Technology, Virginia Tech  
(w) 540-231-7893 (m) 210-264-1185  
t.weeks@vt.edu  

* Tommy Regan  
Senior Director, Information Systems  
(w) 540-231-7539  
tregan@vt.edu  

## Student Information

This is the contact information for the students who
worked on and completed this project.  

* Nick Falls  
EE Student  
fnick@vt.edu  

* Tyler Grim  
EE Student  
tgrim@vt.edu  

* Hamid Hafiz  
EE Student  
hamid14@vt.edu  

* Chuan Xiao  
EE Student  
xcc@vt.edu  